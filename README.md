# gtfs-archive

Archive of LA Metro GTFS releases.

# Info

`gtfs_bus.zip` in the root folder will be the GTFS file containing upcoming service changes.

Upcoming and past GTFS files will be listed in folders named in `YYYY-MM-DD` format, with the date being the service change effective date.

# Updates

## 7/14/2021

Uploaded new GTFS files for upcoming 7/18/21 service changes into the `2021-07-18` folder.
